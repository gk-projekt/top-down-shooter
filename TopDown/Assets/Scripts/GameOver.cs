using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

//class responsible for handling the game over event
public class GameOver : MonoBehaviour
{
    //boolean determining if the game has been already ended
    bool gameEnded;

    private void Awake()
    {
        GetComponent<GameOverEvent>().OnGameOver += GameOver_OnGameOver;
    }

    //stops the player control and shows the game end message, either win or lose depending on message attached to an event
    private void GameOver_OnGameOver(object sender, GameOverEvent.OnGameOverEventArgs e)
    {
        gameEnded = true;
        TextMeshPro gameOverText = GetComponent<TextMeshPro>();
        gameOverText.text = e.message;
    }

    // Start is called before the first frame update
    void Start()
    {
        gameEnded = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameEnded)      //if the game has ended and player presses R it restarts the game
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }
}
