using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//pickup item interface
public interface IItem
{
    //item name identifier
    string ItemName { get; }
    //item assigned object color
    Vector3 color { get; }
    //bullet shoot speed for item type if it changes shooting type
    float shootSpeed { get; }
    //item bullet damage if it changes shooting type
    int damage { get; }
    //item shooting function if it changes shooting type
    void Shoot(Transform shootingPoint, Transform pfBullet, Transform playerPosition);
    
}
