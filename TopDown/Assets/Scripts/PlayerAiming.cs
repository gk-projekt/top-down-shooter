using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class responsible for handling mouse movement and turning it into player aiming
public class PlayerAiming : MonoBehaviour
{
    //camera object
    Camera cam;
    //mouse to camera offset
    [SerializeField]
    CameraOffset camOffset;
    //mouse position
    Vector3 worldPos;
    //aiming point on the player
    private Transform aimTransform;
    // Start is called before the first frame update
    void Start()
    {
        aimTransform = GetComponent<Transform>();
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Player>().isAlive)
        {
            Vector3 mousePos = Input.mousePosition;
            //set the z value of mouse position to offset from camera
            mousePos.z = camOffset.RuntimeOffset;
            worldPos = cam.ScreenToWorldPoint(mousePos);
            //rotate the player towards the cursor
            Vector3 aimDirection = (worldPos - transform.position).normalized;
            float angle = Mathf.Atan2(aimDirection.x, aimDirection.z) * Mathf.Rad2Deg;
            aimTransform.eulerAngles = new Vector3(0, angle, 0);
        }
    }
}
