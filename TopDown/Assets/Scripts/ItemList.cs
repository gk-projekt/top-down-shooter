using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//scriptable object containing a list of items for spawning on stage
[CreateAssetMenu]
public class ItemList : ScriptableObject, ISerializationCallbackReceiver
{
    //item type list
    [NonSerialized]
    List<IItem> itemList;

    //adding item types to the list
    public void OnAfterDeserialize()
    {
        itemList = new List<IItem>();
        itemList.Add(new StandardShot());
        itemList.Add(new TripleShot());
        itemList.Add(new RapidShot());
        itemList.Add(new HealingItem());
    }

    public void OnBeforeSerialize()
    {

    }

    //get item by it's name
    public IItem getItemByID(string id)
    {
        return itemList.Find(it => it.ItemName == id);
    }

    //get random item from the list
    public IItem getRandomItem()
    {
        return itemList[UnityEngine.Random.Range(0, itemList.Count)];
    }
}
