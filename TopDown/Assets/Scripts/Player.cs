using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class responsible for all player actions
public class Player : MonoBehaviour
{
    //player health
    [SerializeField]
    int health;
    //model material for changing color
    [SerializeField]
    Material modelMaterial;
    //bullet material for changing bullet color to be the same as the player
    [SerializeField]
    Material bulletMaterial;
    //point light for changing color of the light to match the player material
    [SerializeField]
    Light pointLight;
    //currently held shooting item type
    public IItem heldItem;
    //determines if player took damage and is invincible for a short amount of time
    public bool isInvincible;
    //invincibility time after taking damage
    private float invincibilityTime;
    //determines if player has any health left
    public bool isAlive;
    //player's max health value
    int maxHealth;

    // Start is called before the first frame update
    void Start()
    {
        isInvincible = false;
        heldItem = new StandardShot();  //set the default item as standard shot
        changeColors(heldItem.color);
        isAlive = true;
        maxHealth = health;
    }
    //returns player's health
    public int getHealth()
    {
        return health;
    }
    //equip a new picked up item
    public void Equip(IItem item)
    {
        if (isAlive)
        {
            if (item.ItemName == "Healing")     //if the item is healing, instead of changing shooting type, heal the character
            {
                health += 8;
                if (health > maxHealth) health = maxHealth;
            }
            else
            {
                heldItem = item;
                changeColors(heldItem.color);
            }
        }
    }
    //if player is touched by enemy or projectile it takes damage and turns invincible for a small duration
    public void takeDamage(int damage)
    {
        if (isAlive)
        {
            if (isInvincible == false)
            {
                health -= damage;
                modelMaterial.SetVector("_Color", new Vector3(40, 40, 40));     //change the color of hero to white to indicate invincibility
                if (health <= 0)
                {
                    isAlive = false;        //if player runs out of health, they die
                }
                isInvincible = true;
                invincibilityTime = 0f;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            invincibilityTime += Time.deltaTime;
            if (isInvincible == true && invincibilityTime > 2f)     //check invincibility timer
            {
                isInvincible = false;
                modelMaterial.SetVector("_Color", heldItem.color);
            }
        }
    }
    //changing colors of player character, bullets and point light after picking up an item
    void changeColors(Vector3 color)
    {
        modelMaterial.SetVector("_Color", color);
        bulletMaterial.SetVector("_Color", color);
        Color col = new Color(color.x, color.y, color.z , 1);
        pointLight.color = col;
    }
    //if enemy notices player via vision collider, change it's mode to aggressive
    private void OnTriggerEnter(Collider other)
    {
        IEnemy enemy = other.GetComponent<IEnemy>();
        if (enemy != null)
        {
            enemy.turnAggressive();
        }
    }
}
