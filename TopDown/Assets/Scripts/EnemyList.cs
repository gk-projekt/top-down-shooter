using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//scriptable object responsible for storing enemy types to spawn randomly onto level
[CreateAssetMenu]
public class EnemyList : ScriptableObject, ISerializationCallbackReceiver
{
    //enemy type list
    [SerializeField]
    List<GameObject> enemyList;

    public void OnAfterDeserialize()
    {

    }

    public void OnBeforeSerialize()
    {
        
    }

    //get random enemy type to spawn on level
    public GameObject getRandom()
    {
        return enemyList[UnityEngine.Random.Range(0, enemyList.Count)];
    }
}
