using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class responsible for handling player input and turning it into movement
[RequireComponent(typeof(Rigidbody))]
public class Movement : MonoBehaviour
{
    //object physics
    Rigidbody rb;
    //player move speed
    [SerializeField]
    float speed;
    //player move speed after taking damage and getting into "panic boost"
    float panicSpeed;
    //last registered player input
    Vector2 lastInput;
    //player object
    Player plr;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        panicSpeed = speed * 1.4f;
        plr = GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        //getting last player input
        lastInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        //if escape is pressed, quit the game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    //handling movement
    private void FixedUpdate()
    {
        if (plr.isAlive)        //move only if player is alive
        {
            if (!plr.isInvincible)      //if player has not taken any dam
            {
                rb.velocity = new Vector3(lastInput.x * speed * Time.deltaTime,
                                    rb.velocity.y, lastInput.y * speed * Time.deltaTime);
            }
            else
            {
                //if player took damage and has temporary invincibility use panic speed boost
                rb.velocity = new Vector3(lastInput.x * panicSpeed * Time.deltaTime,
                                    rb.velocity.y, lastInput.y * panicSpeed * Time.deltaTime);
            }
        }
        else
        {
            //if player is dead it stays still
            rb.velocity = new Vector3(0, 0, 0);
        }
    }
}
