using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class responsible for enemy projectile behaviour
public class EnemyProjectile : MonoBehaviour
{
    //projectile move speed
    [SerializeField]
    float speed;
    //projectile damage to player
    int projectileDamage;

    // Start is called before the first frame update
    public void Setup(Vector3 direction, float flightTime, int damage)
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        //add force to the object into certain direction to make it move
        Vector3 tmpdir = direction;
        rb.AddForce(transform.forward * speed, ForceMode.Impulse);
        projectileDamage = damage;

        Destroy(gameObject, flightTime);
    }

    //if projectile hits player they take damage and projectile is destroyed, if it hits wall it just gets destroyed
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerModel")
        {
            Player player = other.transform.parent.GetComponent<Player>();
            if (player != null)
            {
                player.takeDamage(projectileDamage);
                Destroy(gameObject);
            }
            
        }
        else if(other.tag == "Wall")
            Destroy(gameObject);

    }
}
