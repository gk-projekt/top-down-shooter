using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class responsible for determining a point that is being followed by the camera
public class CameraAnchor : MonoBehaviour
{
    //camera object
    [SerializeField]
    Camera cam;
    //player object
    GameObject player;
    //mouse distance from camera, it's being set as anchor's z value
    [SerializeField]
    CameraOffset camOffset;
    //mouse position in a scene
    Vector3 worldPos;
    //anchor point transform
    Transform tran;
    // Start is called before the first frame update
    void Start()
    {
        tran = GetComponent<Transform>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (player.GetComponent<Player>().isAlive)  //manage anchor movement only if the player is alive
        {
            //determine mouse position in 2d space
            Vector3 mousePos = Input.mousePosition;
            //set mouse distance from camera, by default it is set to 0
            mousePos.z = camOffset.RuntimeOffset;
            worldPos = cam.ScreenToWorldPoint(mousePos);
            //set anchor point position as the middle point between mouse and player position
            tran.position = (worldPos + player.transform.position) / 2;
        }
    }
}
