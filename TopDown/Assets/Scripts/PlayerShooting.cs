using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class responsible for handling the shooting event
public class PlayerShooting : MonoBehaviour
{
    //player bullet prefab
    [SerializeField] 
    private Transform pfBullet;
    //player point from which the bullet is fired
    [SerializeField]
    private GameObject shootingPoint;
    //time interval between shots
    private float shootingTime;

    // Start is called before the first frame update
    private void Awake()
    {
        GetComponent<ShootingEvent>().OnShotFired += PlayerShooting_OnShotFired;
        shootingTime = 0;
    }
    //if the player is shooting use the item's shot type to create a proper bullet
    private void PlayerShooting_OnShotFired(object sender, ShootingEvent.OnShotFiredEventArgs e)
    {
        if (GetComponent<Player>().isAlive)
        {
            if (shootingTime > GetComponent<Player>().heldItem.shootSpeed)
            {
                GetComponent<Player>().heldItem.Shoot(shootingPoint.transform, pfBullet, transform);
                shootingTime = 0;
            }
        }
    }
    //update the time between shots
    private void Update()
    {
        shootingTime += Time.deltaTime;
    }
}
