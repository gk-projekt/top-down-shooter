using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class responsible for player bullet behaviour
public class BulletPhysics : MonoBehaviour
{
    //bullet move speed
    [SerializeField]
    float speed;
    //bullet damage dealt to enemies
    int bulletDamage;
    // Start is called before the first frame update
    public void Setup(Vector3 direction, float flightTime, int damage)
    {
        Rigidbody rb = GetComponent<Rigidbody>();

        //add force to an object into certain direction to make it move
        Vector3 tmpdir = direction;
        rb.AddForce(transform.forward * speed, ForceMode.Impulse);
        bulletDamage = damage;
        float angle = Mathf.Atan2(tmpdir.z, tmpdir.x) * Mathf.Rad2Deg;
        if (angle < 0) angle += 360;
        transform.eulerAngles = new Vector3(0, 0, angle);

        //destroy a bullet after set time if it doesn't hit anything
        Destroy(gameObject, flightTime);
    }

    //if the bullet hits enemy they take damage, if it hits the wall it only destroys itself
    private void OnTriggerEnter(Collider other)
    {
        IEnemy enemy = other.GetComponent<IEnemy>();
        if (enemy != null)
        {
            enemy.takeDamage(bulletDamage);
            Destroy(gameObject);
        }
        else if (other.tag=="Wall")
        {
            Destroy(gameObject);
        }
    }
}
