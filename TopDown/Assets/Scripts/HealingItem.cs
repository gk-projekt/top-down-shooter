using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//healing pickup item class, 
public class HealingItem : IItem
{
    //item name
    private string itemName = "Healing";
    //public item name
    public string ItemName { get => itemName; }
    //item object color
    public Vector3 color { get { return new Vector3(40, 0, 40); } }
    //item shot speed, not important for healing item
    public float shootSpeed { get => 0.3f; }
    //item damage, not important for healing item
    public int damage { get => 4; }

    public void Shoot(Transform shootingPoint, Transform pfBullet, Transform playerPosition)
    {
    }

}
