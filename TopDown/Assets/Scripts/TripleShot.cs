using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//item type changing shooting type to shooting 3 bullets at the same time in a slight arc
public class TripleShot : IItem
{
    //item name
    private string itemName="Triple shot";
    //item name for checking
    public string ItemName { get =>itemName;  }
    //item material color
    public Vector3 color { get { return new Vector3(40, 0, 0); } }
    //item shot speed
    public float shootSpeed { get => 0.5f; }
    //item bullet damage
    public int damage { get => 3; }

    //shooting type: slower speed, slightly smaller damage, 3 bullets shot in similar direction
    public void Shoot(Transform shootingPoint, Transform pfBullet, Transform playerPosition)
    {
        Vector3 playerPos = shootingPoint.transform.position;
        Vector3 shootDir = playerPos.normalized;
        //prepare and shoot 3 shots with slightly different angle
        for (int i = -1; i < 2; i++)
        {
            //set the shot rotation
            Quaternion rotation = playerPosition.rotation;
            rotation.eulerAngles = new Vector3(rotation.eulerAngles.x, rotation.eulerAngles.y + i*10, rotation.eulerAngles.z);
            //create and setup a new bullet
            Transform bulletTransform = GameObject.Instantiate(pfBullet, playerPos, rotation);
            bulletTransform.GetComponent<BulletPhysics>().Setup(shootDir, 1f, damage);
        }
    }

}
