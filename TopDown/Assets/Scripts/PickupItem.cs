using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class responsible for setting the pickup item type to a prefab
public class PickupItem : MonoBehaviour
{
    //item list to choose a type from
    [SerializeField]
    ItemList itemList;
    //item model to determine to color of material
    [SerializeField]
    GameObject itemModel;
    //item type for holding the information about the item
    private IItem itemType;

    // Start is called before the first frame update
    void Start()
    {
        itemType = itemList.getRandomItem();    //choose a random item from the list
        Material itemMaterial = itemModel.GetComponent<Renderer>().material;    //get prefab material
        itemMaterial.SetVector("_Color", itemType.color);   //change material's color
    }

    //picking up the item by player
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerModel")
        {
            Player player = other.transform.parent.GetComponent<Player>();
            if (player != null)
            {
                player.Equip(itemType); //if player touches the item, equip it and destroy the item on the ground
                Destroy(gameObject);
            }
        }
    }
}
