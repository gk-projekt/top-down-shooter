using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class responsible for all the actions of archer enemy
public class Archer : MonoBehaviour, IEnemy
{
    //enemy name identifier
    public string enemyName { get => "Archer"; }
    //enemy health from interface
    private int health;

    //enemy public health
    public int Health
    {
        get { return health; }
        set
        {
            if (value < 0) health = 0;
            if (value > 100) health = 100;
            health = value;
        }
    }

    //damage the enemy deals to a player
    public int damage { get => 4; }

    //boolean determining if enemy is aggressive towards player
    private bool isAggressive;

    //player variable, mostly for checking position
    Player player;

    //projectile prefab used by archer to shoot
    [SerializeField]
    Transform pfBullet; 

    //time between 
    private float reloadTime;

    //time between shots
    private float shootTime;

    //whenever enemy takes damage, the health is reduced by that amount
    public void takeDamage(int takenDamage)
    {
        Health -= takenDamage;
        if (health <= 0) die();
    }

    //actions taken by enemy the moment their health reaches 0
    public void die()
    {
        //checking if all the enemies were defeated so the boss can spawn
        GameObject.FindGameObjectWithTag("Generator").GetComponent<LevelGeneration>().SpawnBoss();
        //destroying the object
        Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        Health = 20;
        isAggressive = false;
        reloadTime = 0;
        shootTime = 3f;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    //check if player has touched the archer, if they did they take enemy damage
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerModel")
        {
            Player player = other.transform.parent.GetComponent<Player>();
            if (player != null)
            {
                player.takeDamage(damage);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isAggressive)  //if enemy sees player take action
        {
            reloadTime += Time.deltaTime;
            //determining shot direction and turning the enemy towards the player
            Vector3 direction = player.transform.position - transform.position;
            float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            transform.eulerAngles = new Vector3(0, angle, 0);
            if (reloadTime > shootTime) //if the reload time has passed take the shot and reset reloat timer
            {
                shoot();
                reloadTime = 0;
            }
        }
    }

    //if enemy sees player they change to aggressive mode
    public void turnAggressive()
    {
        isAggressive = true;
    }

    //create a projectile prefab copy that gets shot at player and uses enemy damage
    public void shoot()
    {
        //create a projectile
        Vector3 enemyPos = transform.position;
        Transform bulletTransform = Instantiate(pfBullet, enemyPos, transform.rotation);

        //setup a projectile
        Vector3 shootDir = enemyPos.normalized;
        bulletTransform.GetComponent<EnemyProjectile>().Setup(shootDir, 3f, damage);
    }
}
