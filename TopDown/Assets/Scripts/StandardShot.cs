using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//item type changing shooting type to standard shot
public class StandardShot : IItem
{
    //item name
    private string itemName = "Standard shot";
    //item name for checking
    public string ItemName { get => itemName; }
    //item material color
    public Vector3 color { get { return new Vector3(0, 40, 40); } }
    //item shoot speed
    public float shootSpeed { get => 0.3f; }
    //item bullet damage
    public int damage { get => 4; }

    //shooting type: standard speed, standard damage, no recoil
    public void Shoot(Transform shootingPoint, Transform pfBullet, Transform playerPosition)
    {
        //create a new bullet from a shooting point
        Vector3 playerPos = shootingPoint.transform.position;
        Transform bulletTransform = GameObject.Instantiate(pfBullet, playerPos, playerPosition.rotation);
        //setup bullet to fly
        Vector3 shootDir = playerPos.normalized;
        bulletTransform.GetComponent<BulletPhysics>().Setup(shootDir, 3f, damage);
    }

}