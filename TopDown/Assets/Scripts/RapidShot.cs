using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//item type changing shooting type to rapid fire but dealing less damage and having worse accuracy
public class RapidShot : IItem
{
    //item name
    private string itemName = "Rapid shot";
    //item name for identifying
    public string ItemName { get => itemName; }
    //item material color
    public Vector3 color { get { return new Vector3(0, 40, 0); } }
    //bullet shot speed
    public float shootSpeed { get => 0.1f; }
    //item bullet damage
    public int damage { get => 1; }
    //shooting type: rapid fire, small damage, random recoil
    public void Shoot(Transform shootingPoint, Transform pfBullet, Transform playerPosition)
    {
        //determine recoil of the next shot (angle between -10 to 10 degrees of actual target)
        float recoil = Random.Range(-10.0f,10.0f);
        Vector3 playerPos = shootingPoint.transform.position;
        Quaternion rotation = playerPosition.rotation;
        rotation.eulerAngles = new Vector3(rotation.eulerAngles.x, rotation.eulerAngles.y+recoil, rotation.eulerAngles.z);
        //create a new bullet from a shooting point
        Transform bulletTransform = GameObject.Instantiate(pfBullet, playerPos, rotation);
        //setup a bullet to fly
        Vector3 shootDir = playerPos.normalized;
        bulletTransform.GetComponent<BulletPhysics>().Setup(shootDir, 3f, damage);
    }

}