using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//scriptable object for setting constant camera to mouse offset for a whole project
[CreateAssetMenu]
public class CameraOffset : ScriptableObject, ISerializationCallbackReceiver
{
    //developer set offset
    [SerializeField]
    private float InitialOffset;

    //offset given to scripts
    [NonSerialized]
    public float RuntimeOffset;

    public void OnAfterDeserialize()
    {
        RuntimeOffset = InitialOffset;
    }

    public void OnBeforeSerialize() { }
}