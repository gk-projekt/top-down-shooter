using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class responsible for health bar control
public class HealthBar : MonoBehaviour
{
    //health bar object
    [SerializeField]
    GameObject bar;
    //player class
    Player player;
    //player's max health
    float maxHealth;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        maxHealth = player.getHealth();
    }

    // Update is called once per frame
    void Update()
    {
        //the bar's length is adjusted to match the amount of health the player has
        bar.transform.localScale = new Vector3(player.getHealth() / maxHealth, bar.transform.localScale.y, bar.transform.localScale.z);
    }
}
