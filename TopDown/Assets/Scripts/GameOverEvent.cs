using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class checking for game over event to happen
public class GameOverEvent : MonoBehaviour
{
    //player class
    Player player;
    //determining if game has been won
    bool gameEnded;
    //event handler
    public event EventHandler<OnGameOverEventArgs> OnGameOver;
    public class OnGameOverEventArgs : EventArgs
    {
        //message shown on screen after finishing game
        public string message;
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        gameEnded = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (!player.isAlive&&!gameEnded)    //if the player died and the game was still running that means the player lost
        {
            OnGameOver?.Invoke(this, new OnGameOverEventArgs { message = "Game Over\n<size=1>Press R to restart" });
        }
        if (gameEnded)      //if the game has ended and player is alive that means the player won
        {
            player.isAlive = false;     //set player as dead to stop it from taking input
            OnGameOver?.Invoke(this, new OnGameOverEventArgs { message = "YOU WON\n<size=1>Press R to restart" });
        }
    }

    //finish the game
    public void endGame()
    {
        gameEnded = true;
    }
}
