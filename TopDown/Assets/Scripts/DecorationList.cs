using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//scriptable object responsible for storing a list of decorative objects for level random creation
[CreateAssetMenu]
public class DecorationList : ScriptableObject, ISerializationCallbackReceiver
{
    //decoration object list
    [SerializeField]
    List<GameObject> decorationList;

    public void OnAfterDeserialize()
    {
        
    }

    public void OnBeforeSerialize()
    {
        
    }

    //get a random object from a list to put into level
    public GameObject getRandom()
    {
        return decorationList[UnityEngine.Random.Range(0, decorationList.Count)];
    }

}
