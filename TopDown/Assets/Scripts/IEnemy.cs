using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//enemy interface
public interface IEnemy
{
    //enemy name
    string enemyName { get; }
    //enemy health
    int Health { get; set; }
    //enemy damage
    int damage { get; }

    //taking damage
    void takeDamage(int takenDamage);
    //changing mode after seeing the player
    void turnAggressive();
    //enemy death action
    void die();
}
