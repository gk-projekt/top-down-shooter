using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class responsible for all boss actions
public class Boss : MonoBehaviour, IEnemy
{
    //enemy name identifier
    public string enemyName { get => "Boss"; }
    //health variable from interface
    private int health;

    //variable responsible for enemy physics
    Rigidbody rb;

    //player variable for determining direction
    Player player;

    //enemy movement speed
    [SerializeField]
    float speed;

    //enemy move direction
    Vector3 moveDirection;

    //variable determining if boss sees player
    private bool isAggressive;

    //public health variable
    public int Health
    {
        get { return health; }
        set
        {
            if (value < 0) health = 0;
            if (value > 100) health = 100;
            health = value;
        }
    }

    //enemy damage it deals to player via contact or projectile
    public int damage { get => 4; }

    //projectile prefab
    [SerializeField]
    Transform pfBullet;

    //current reload time
    private float reloadTime;

    //time needed to take a shot
    private float shootTime;

    //whenever enemy takes damage, the health is reduced by that amount
    public void takeDamage(int takenDamage)
    {
        Health -= takenDamage;
        if (health <= 0) die();
    }

    //actions taken by enemy the moment their health reaches 0
    public void die()
    {
        //if the boss is defeated, trigger the game end
        GameObject.FindGameObjectWithTag("Finish").GetComponent<GameOverEvent>().endGame();
        Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        Health = 100;
        rb = GetComponent<Rigidbody>();
        isAggressive = false;
        reloadTime = 0;
        shootTime = 3f;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    //check if player has touched the archer, if they did they take enemy damage
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerModel")
        {
            Player player = other.transform.parent.GetComponent<Player>();
            if (player != null)
            {
                player.takeDamage(damage);
            }
        }
    }

    //if enemy sees player they change to aggressive mode
    public void turnAggressive()
    {
        isAggressive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isAggressive)
        {
            //determining move direction and transform angle for boss
            moveDirection = player.transform.position - transform.position;
            float angle = Mathf.Atan2(moveDirection.x, moveDirection.z) * Mathf.Rad2Deg;
            transform.eulerAngles = new Vector3(0, angle, 0);
            moveDirection.Normalize();

            reloadTime += Time.deltaTime;
            if (reloadTime > shootTime) //checking if boss can take a shot
            {
                shoot();
                reloadTime = 0;
            }
        }
    }

    //enemy movement in chosen direction
    void FixedUpdate()
    {
        if (isAggressive)
        {
            rb.velocity = new Vector3(moveDirection.x * Time.deltaTime * speed,
                                        rb.velocity.y,
                                        moveDirection.z * Time.deltaTime * speed);
        }
    }

    //boss shoots 6 projectiles around itself, one always aims the player
    public void shoot()
    {
        for (int i = 0; i < 6; i++)
        {
            //determine the angle;
            Quaternion rotation = transform.rotation;
            rotation.eulerAngles = new Vector3(rotation.eulerAngles.x, rotation.eulerAngles.y + i * 60, rotation.eulerAngles.z);

            //create a projectile
            Vector3 enemyPos = transform.position;
            Transform bulletTransform = Instantiate(pfBullet, enemyPos, rotation);

            //setup a projectile
            Vector3 shootDir = enemyPos.normalized;
            bulletTransform.GetComponent<EnemyProjectile>().Setup(shootDir, 3f, damage);
        }
    }
}
