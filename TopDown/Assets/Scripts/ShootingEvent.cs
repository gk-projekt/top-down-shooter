using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class for catching an event of mouse input to shoot
public class ShootingEvent : MonoBehaviour
{
    //camera object
    Camera cam;
    //mouse to camera offset
    [SerializeField]
    CameraOffset camOffset;
    //mouse position
    private Vector3 worldPos;

    public event EventHandler<OnShotFiredEventArgs> OnShotFired;
    public class OnShotFiredEventArgs : EventArgs
    {
        //event sends the mouse position to the shooter
        public Vector3 worldPos;
    }

    // Start is called before the first frame update
    void Start()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        //if player presses left mouse button send an information with mouse position on screen
        if (Input.GetMouseButton(0))
        {
            Vector3 mousePos = Input.mousePosition;

            mousePos.z = camOffset.RuntimeOffset;
            worldPos = cam.ScreenToWorldPoint(mousePos);
            OnShotFired?.Invoke(this, new OnShotFiredEventArgs { worldPos = worldPos });
        }
    }
}
