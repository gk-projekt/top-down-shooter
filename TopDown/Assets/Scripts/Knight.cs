using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class responsible for knight enemy type actions
public class Knight : MonoBehaviour, IEnemy
{
    //enemy name identifier
    public string enemyName { get => "Knight"; }
    //enemy health 
    private int health;
    //physics for movement
    Rigidbody rb;
    //player class for determining move direction
    Player player;
    //move speed
    [SerializeField]
    float speed;
    //move direction (towards player)
    Vector3 moveDirection;

    //determining if enemy sees player
    private bool isAggressive;
    //public enemy health
    public int Health
    {
        get { return health; }
        set
        {
            if (value < 0) health = 0;
            if (value > 100) health = 100;
            health = value;
        }
    }
    //damage dealt to player in contact
    public int damage { get => 4; }
    //taking damage from player, if health reaches 0 trigger death function
    public void takeDamage(int takenDamage)
    {
        Health -= takenDamage;
        if (health <= 0) die();
    }
    //on death actions
    public void die()
    {
        //check if boss can be spawned
        GameObject.FindGameObjectWithTag("Generator").GetComponent<LevelGeneration>().SpawnBoss();
        Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        Health = 40; //starting health
        rb = GetComponent<Rigidbody>();
        isAggressive = false;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }
    //in contact with player, player takes damage
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerModel")
        {
            Player player = other.transform.parent.GetComponent<Player>();
            if (player != null)
            {
                player.takeDamage(damage);
            }
        }
    }
    //if knight notices player, change the value
    public void turnAggressive()
    {
        isAggressive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isAggressive)
        {
            //if enemy sees player, determine move direction towards them
            moveDirection = player.transform.position - transform.position;
            float angle = Mathf.Atan2(moveDirection.x, moveDirection.z) * Mathf.Rad2Deg;
            transform.eulerAngles = new Vector3(0, angle, 0);
            moveDirection.Normalize();
            
        }
    }
    //movement towards player
    void FixedUpdate()
    {
        if (isAggressive)
        {
            rb.velocity = new Vector3(moveDirection.x * Time.deltaTime * speed,
                                        rb.velocity.y,
                                        moveDirection.z * Time.deltaTime * speed);
        }
    }
}
